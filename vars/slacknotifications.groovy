def call(Map config = [:]){
    slackSend channel: "${config.channel}", message: "Job name $JOB_NAME with build number $BUILD_NUMBER and display name $BUILD_DISPLAY_NAME is ${config.msg} having build url $BUILD_URL"
}
